\documentclass[10pt]{scrartcl}

\usepackage{xeCJK}
\setCJKmainfont{Noto Serif CJK TC}

\usepackage{setspace}
\onehalfspacing

\usepackage[a4paper,margin=1cm,footskip=20pt]{geometry}
\usepackage{epsfig,amsmath,amssymb,latexsym,cancel}
\usepackage{graphicx,epsfig,color,epsf,psfrag,hhline,amsmath,amssymb,textcomp,bigints}
\usepackage{enumitem}
\usepackage{hyperref,url,xcolor}

\usepackage{graphicx,wrapfig,multicol}
\usepackage{lipsum}

\usepackage[boxruled]{algorithm2e}

\newcommand{\see}[1]{(see \texttt{#1})}
\newcommand{\pins}[2]{\texttt{#1} $#2$}
\renewcommand*\sectionformat{}
\SetFuncSty{textsc}
\renewcommand{\algorithmautorefname}{Algorithm}

\pagestyle{plain}
\begin{document}

\textbf{Online Judge account: }\texttt{manaka\_ao}, 20 AC out of 20 test cases

\textbf{黃明瀧 107021129}\hfill 2020-04-19

\singlespacing
{\color[HTML]{A13D2D}\textbf{Note to TAs: } Since in the announcement we have ``需與上傳Online Judge的Code相同'', the code I upload will be a uglified version with all lines condensed into one single line. This is due to the limit of Online Judge, where the code can't exceed 40 KiB in total, or \textbf{it won't let me submit} (It shows a red error message instead). My code is approx. 55 KiB in total. \href{https://gitlab.com/Akitaki/i2p-mini-compiler/}{On my git repo} there's a copy of the original version, which is a lot easier to read.}

I would actually call this mini project a \textit{compiler} instead of a \textit{calculator}. It's essentially a tiny compiler that consists of only one basic block, with very basic grammar rule. I decided to try out some ideas that I've read about before, but have yet implemented on my own. The following will be a brief introduction of the design of my tiny compiler.

\begin{multicols}{2}
    \begin{wrapfigure}{L}[8pt]{0.2\textwidth}
        \includegraphics[width=0.2\textwidth]{diagram.png}
        \caption{Data flow in my code.}
        \label{fig:dataflow}
        Steps in bold font are necessary, while other steps are optional optimizations.
    \end{wrapfigure}

    \autoref{fig:dataflow} is a brief diagram for the compiler's execution process. I modified the original template to a more splitted scheme, instead of combining all the steps together\footnote{Admittedly, I didn't read the template very carefully.}. The execution is split into these steps:

    \begin{enumerate}
        \item \textbf{The lexer} \see{lex()} first read raw string input from STDIN, and tokenize them. This part is pretty simple, and the only check done here is character set check, i.e. if in the input there are bytes that aren't in valid, it'll abort the program.
        \item The \textbf{constructor of ASTs} \see{ast\_build()} then reads the previously generated vector of tokens to generate syntax trees. This part is written in a manner similar to the original template, using the same right-recursion grammar given in the spec. In this phase, more semantic checks are done, such as unexpected end of input, undefined identifiers, or unexpected tokens. Also, the identifier names such as \texttt{x, y, z} are replaced by numerical IDs starting from 0.
    \end{enumerate}

    \begin{enumerate}
            \setcounter{enumi}{2}
        \item These ASTs are later \textbf{translated to an in-memory IR\footnote{IR = intermediate representation} format} represented by a structure \see{irvector\_push\_from\_astnode()}. The reason for generating IR first instead of directly emitting assembly output is because that the assembly syntax actually adds complexities, since all the instruction formats constist of only two operands. This makes it difficult to perform tasks like $t_1 \gets t_2 + t_3$, where we probably need to split it into two instructions, \pins{MOV}{t_1, t_2} and \pins{ADD}{t_1, t_3}. My IR format includes these pseudo-instructions:
            \begin{itemize}
                \item \texttt{IRSet(dest, const)} sets register's value to a constant.
                \item \texttt{IRAdd(dest, source1, source2), IRSub(dest, source1, source2)} and other arithmetic pseudo-instructions all have three operands instead of two. This makes it easier to express operations where the destination is different from the source.
                \item \texttt{IRMov(dest, source)} corresponds to the \texttt{Mov} instruction directly.
                \item \texttt{IRSave(dest, source)}, a special directive to store final values of $x$, $y$, and $z$ into registers \texttt{r0} ~ \texttt{r2}.
            \end{itemize}

            One thing worth noting is that \textbf{there are no register-to-memory load directive}, because during translation from ASTs to IR, we convert them into \textbf{\href{https://en.wikipedia.org/wiki/Static_single_assignment_form}{SSA-form} representation} of the original input program.
        \item Finally, the generated vector of IR are converted into \textbf{assembly instructions}. This phase involves a linear-scan register allocation process to assign temporary variables into registers.
    \end{enumerate}
\end{multicols}

\section{More Implementation Details}

With the brief sketch given above, I'd like to elaborate more on the implementation, as well as \textbf{why} I design them like this. The optimizations phases will also be explained below.

\begin{multicols*}{2}

    \subsection{The Container Types}
    This is actually the first time in which I write so many container types purely in C. In the beginning of doing this mini project, I actually first wrote proof-of-concept lexer and AST classes in C++ to confirm that my algorithms are correct. These code are later translated into C due to the limitation of the homework, but I retained a large portion of the original object-oriented semantics. Owing to the absence of template programming tools in C, there are \textbf{a lot} of repeating boilerplate code all over the project. For instance, there are a whooping number of 7 vector-like structure definitions: \texttt{string, vector, boolvector, tokenvector, astvector, irvector, insvector}. This is one of the main reasons why my code is too long to be submitted to the OJ.
    \subsection{Constant Folding and Propagation}
    In \autoref{fig:dataflow}, we see that the first optimization is constant folding combined with constant propagation. Observe that there are \textbf{no branch or jump instructions} in the assembly language, constant propagation is pretty trivial. The constant folding function(s) iterates over the list of ASTs. For every AST, it recursively traverse all the vertices in bottom-up direction, substituting those vertices that can be replaced by a constant node instead. A plain boolean array \texttt{consttbl} is used to mark those identifier IDs that are known to be constant at the moment. The pseudocode is shown in \autoref{alg:constfold}.
    \subsection{Dead Code Elimination}
    Dead code elimination is also performed on the list of ASTs. Again, since there are no branch or jump instructions, dead code elimination is also doable. The dead code elimination function iterates over the list of ASTs \textbf{in reversed order}. For every AST, it first check if the write destination is being read by the subsequent code below it, and mark them as "useless" if not. A plain boolean array \texttt{read\_table} is used as flags for read and write operations. The pseudocode is shown in \autoref{alg:deadcode}.
    \subsection{Linear Scan Register Allocation}
    From my understanding, the register allocation problem can be reduced to the graph-coloring problem, which is \textbf{NP-complete} (oops). Originally, I was going to implement the Chaitin's algorithm to color the interference graph, but due to my laziness I switched to a more naive linear scan algorithm. The IR is already in SSA form, so presumably most variables shouldn't have very long lifetimes. Therefore, the linear algorithm produces results that are acceptable. The pseudocode is shown in \autoref{alg:linscan}.
    \subsection{Peephole Optimization}
    During testing, I observed that my compiler often generates instructions with such pattern: $t_2 \gets t_3, \, t_1 \gets t_2$. This cause high cycle counts, since they can be replaced with a single instruction $t_1 \gets t_3$ in most cases, if the intermediate variable $t_2$ isn't read afterwards. Therefore, I wrote an additional function to iterate over the list of instructions, comparing each adjacent pairs of them to see if they can be reduced. The pseudocode is omitted since it's rather long and I'm running out of page space.

    \begin{algorithm}[H]
        \label{alg:constfold}
        \caption{Constant folding algorithm}
        \SetKwFunction{FRCF}{RecursiveConstFold}
        \DontPrintSemicolon

        $c_1, \dots, c_n \gets$ false$,\dots,$ false\;
        $w_1, \dots, w_n \gets 0, \dots, 0$\;
        \For{AST $T$ in list of ASTs}{
            $v \gets$ root vertice of $T$\;
            call \textsc{RecursiveConstFold}(right child of $v$)\;
            $i \gets $ identifier of $v$\;
            \uIf{right child of $v$ is constant}{
                $c_i \gets true$ and $w_i \gets$ value of left child\;
            }
            \Else{
                $c_i \gets false$
            }
        }
        \setcounter{AlgoLine}{0}
        \SetKwProg{myproc}{Procedure}{}{}
        \SetKwProg{Pn}{Function}{:}{\KwRet}
        \Pn{\FRCF{$v$}}{
            \uIf{$v$ is an operator}{
                call \textsc{RecursiveConstFold}(left child of $v$)\;
                call \textsc{RecursiveConstFold}(right child of $v$)\;
                \If{Both children of $v$ is constant}{
                    Abort if it's division by zero\;
                    Calculate result, replace $v$ with constant\;
                }
            }
            \ElseIf{$v$ is an identifier}{
                $i \gets$ identifier of $v$\;
                \If{$c_i$ is true}{
                    Replace $v$ with constant $w_i$\;
                }
            }
        }
    \end{algorithm}

    \phantom{split}

    \begin{algorithm}[H]
        \label{alg:deadcode}
        \caption{Deadcode elimination}
        \DontPrintSemicolon
        \SetNoFillComment

        $read[0 \dots 2] \gets true$\;
        $read[3 \dots 63] \gets false$\;
        \For{AST $T$ from list in reversed order}{
            \uIf{$read[$dest of $T$$]$ is false}{
                \tcc{Write of $T$ isn't read afterwards}
                Mark $T$ as useless\;
            }
            \Else{
                \tcc{Clear read flag}
                $read[$dest of$T] \gets false$\;
            }
            \For{each read source $j$ in $T$}{
                $read[j] \gets true$\;
            }
        }
    \end{algorithm}

    \phantom{split}

    \vspace{-10pt}
    \begin{algorithm}[H]
        \label{alg:linscan}
        \caption{Linear scan register allocation}
        \DontPrintSemicolon
        \SetNoFillComment
        \tcc{"Spill" is actually another function that use some heuristics to choose a victim to kick}

        \For{IR $i$ from list of IRs}{
            Revoke occupied registers with expired variables\;
            Let $d,s_1,s_2$ be the destination and sources\;
            Let $r_d,r_1,r_2 = None$ be the registers to use\;
            \For{$r$ in ${r_d,r_1,r_2}$}{
                \uIf{Has empty register}{
                    $r \gets$ the empty register\;
                }
                \Else{
                    Spill one resident in regiters to memory\;
                    $r \gets$ the spilled register\;
                }
            }
            Translate IR $i$ into instructions with parameters $r_d, r_1, r_2$\;
        }
    \end{algorithm}
    \section{Afterword}
    Prior to doing this project, I had once tried to create a toy programming language when I took the \textit{Formal Languages} course. I wrote a lexer and interpreter, which were somewhat working in the beginning, but I quickly lost interest in it and stopped further development.

    It was a lot of fun to do this mini project, especially because I'm forced (no offence) to use C language instead of some more high level ones. If I have choice, I would probably write it in Rust though, since it's much more easy to organize all the variants (using Rust's \texttt{enum} and \texttt{match} expressions) instead of plain C enum and switch statements. {\scriptsize I hate switch statements. They're so hard to read.}

    The git repository for this project is available at \url{https://gitlab.com/Akitaki/i2p-mini-compiler/}. It includes

    \begin{itemize}
        \setlength\itemsep{0em}
        \item \texttt{main.c}: The original, readable version of the code. If you have to read my code, read this one.
        \item \texttt{main-condensed.c}: Condensed, unreadable version I uploaded to OJ. The same as the one uploaded to iLMS.
        \item \texttt{report.tex}: Source of this document you're reading.
        \item \texttt{simulator}: The simulator I used to test my own code, written in Ruby.
    \end{itemize}
    Thank you for reading both this report and my ugly code written in rush.
\end{multicols*}

\end{document}
